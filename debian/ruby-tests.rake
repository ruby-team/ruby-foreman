require 'gem2deb/rake/spectask'

# Ubuntu builders might not have TERM set which causes some test failures.
ENV["TERM"] = "xterm"

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.exclude_pattern = './spec/foreman/export/**/*_spec.rb' if ENV['AUTOPKGTEST_TMP']
  spec.rspec_opts = '--format doc --backtrace --seed 47932'
end
